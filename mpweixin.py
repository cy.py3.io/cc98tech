import sys
try:
    from EasyLogin import EasyLogin, BeautifulSoup # https://github.com/zjuchenyuan/EasyLogin
except:
    print("[Error] Please download EasyLogin.py and install its dependencies:")
    print("wget https://raw.githubusercontent.com/zjuchenyuan/EasyLogin/master/EasyLogin.py\npip3 install -U requests[socks] bs4 -i https://pypi.doubanio.com/simple/ --trusted-host pypi.doubanio.com")
    exit(1)
import tomd # modified from https://github.com/gaojiuli/tomd
import re
import traceback
import time
import io
try:
    from config import PROXY_OUT_POOL #代理配置，优先使用其第0个代理
except:
    PROXY_OUT_POOL = [""]

__all__ = ["mpweixin_url2md"]
"""
Quick Usage: 快速上手：
命令行直接执行 第一个参数为推文链接，第二个是debug类型
debug=1查看取得的文章内容html所在行；debug=2查看tomd转换得到的markdown内容；debug=3查看最终转换结果
举个例子：
python3 mpweixin.py https://mp.weixin.qq.com/s/-dAJCcfhB9hcnkpItYRI_w 2

Usage Code Example: 作为库使用：

from mpweixin import mpweixin_url2md
from cc98api_base import CC98_API_V2
api = CC98_API_V2(username, password) # 登录CC98 API以便传图
# 你也可以传入一个实现了upload(fp) 返回[url]的对象，查看下方"cc98 api only used here"这一行了解更多
# api也可以为None 但所有的图片链接会变成to be replaced，仅用于测试
md = mpweixin_url2md(url, api)
print(md)
"""


def myprint(*args, **kwargs):
    args = list(args)
    args[0] = "["+time.strftime("%Y-%m-%d %H:%M:%S")+"] " + str(args[0])
    print(*args, **kwargs)

TITLE_RE = re.compile(r'<title>(.*)</title>')
IMG_RE = re.compile(r'<img [^>]+>')
IMG_PLACEHOLDER = "QT9PaPs4ACjk7f8m" # hope this string do not occure in html
IMG_WIDTH_RE = re.compile(r'[;"\s]+width: (\d+)px')
VOICE_RE = re.compile(r'<mpvoice [^>]+></mpvoice>')
VOICE_PLACEHOLDER = "dQWtQWjQ3XNAkXG7"

a=EasyLogin(cachedir="__pycache__") # 缓存文件放入__pycache__里面，正好被.gitignore

if len(PROXY_OUT_POOL):
    PROXY_CUR = PROXY_OUT_POOL[0]
else:
    PROXY_CUR = ""

WEIXIN_IMG_CACHEDICT = {}

def weixin_img_to_cc98(url, api):
    global WEIXIN_IMG_CACHEDICT
    if url in WEIXIN_IMG_CACHEDICT:
        return WEIXIN_IMG_CACHEDICT[url]
    else:
        myprint("[weixinimg_download]", url)
        imgdata = a.get(url, result=False, o=True).content
        myprint("[weixinimg_upload]", url)
        img_src = api.upload(io.BytesIO(imgdata))[0] # cc98 api only used here
        img_src = img_src.replace("http://file.cc98.org", "https://file.cc98.org")
        WEIXIN_IMG_CACHEDICT[url] = img_src
        return img_src

def myconvert(html, debug=False, api=None):
    IMGS=[]
    line = html
    for img in IMG_RE.findall(line): #处理图片，转为PLACEHOLDER，不使用tomd来处理图片
        try:
            img_src = img.split("src=\"")[1].split('"')[0]
        except:
            continue
        if img_src == "":
            continue
        if img_src.startswith("//"):
            img_src = "https:"+img_src
        # fixme: auto resize too large images using their width
        if api is not None:
            img_src = weixin_img_to_cc98(img_src, api) # 下载并上传图片
        else:
            img_src = "to be replaced"
        IMGS.append(img_src)
        line = line.replace(img, IMG_PLACEHOLDER+str(len(IMGS))+IMG_PLACEHOLDER)
    
    VOICES = []
    for voice in VOICE_RE.findall(line):
        voice_src = voice.split("voice_encode_fileid=\"")[1].split('"')[0]
        VOICES.append(voice_src)
        line = line.replace(voice, VOICE_PLACEHOLDER+str(len(VOICES))+VOICE_PLACEHOLDER)
    
    if debug==1: 
        print(line)
    md = tomd.convert(line)
    if debug==2: 
        print(md)
    result = []
    
    for line in md.split("\n"): # 得到md后继续处理图片，将PLACEHOLDER转成上传后得到的url
        if IMG_PLACEHOLDER in line:
            t = 0
            _line = line
            for id in _line.split(IMG_PLACEHOLDER):
                if t%2 == 1:
                    line = line.replace(IMG_PLACEHOLDER+id+IMG_PLACEHOLDER, "![]("+IMGS[int(id)-1]+")\n")
                t += 1
        if VOICE_PLACEHOLDER in line:
            t = 0
            _line = line
            for id in _line.split(VOICE_PLACEHOLDER):
                if t%2 == 1:
                    line = line.replace(VOICE_PLACEHOLDER+id+VOICE_PLACEHOLDER, "```ubb\n[audio]https://ckcqs1401.b0.upaiyun.com/voice/getvoice?mediaid="+VOICES[int(id)-1]+"[/audio]\n```\n")
                t += 1
        result.append(line)
    md = "\n".join(result)
    return md

def mpweixin_url2md(url, api, debug=False):
    global PROXY_CUR
    html = None
    for proxy_candidate in PROXY_OUT_POOL:
        try:
            a.proxies = {"http":PROXY_CUR,"https":PROXY_CUR}
            html = a.get(url, cache=True, result=False, timeout=3)
            break
        except:
            traceback.print_exc()
            PROXY_CUR = proxy_candidate
            myprint("[mpweixin] change proxy to",PROXY_CUR)
            continue
    if html is None:
        raise Exception("cannot connect to outside website")
    
    title = TITLE_RE.findall(html.replace("\n",""))[0].strip()
    flag = False
    lines = []
    nickname = "" #公众号名称
    
    if '<div class="rich_media_content " id="js_content">' in html and ("var first_sceen__time" in html):
        line = html.split('<div class="rich_media_content " id="js_content">')[1].split("var first_sceen__time")[0]
        if 'var nickname = ' in html:
            nickname = html.split('var nickname = ')[1].split('"')[1]
    else:
        for line in html.split("\n"):
            if 'data-src="https://mmbiz.qpic.cn' in line: #这一行有图片，应该就是正文咯
                lines.append(line)
                flag = True
            if 'var nickname = ' in line:
                nickname = line.split('"')[1] # 找到了公众号名称
        if flag:
            line = "\n".join(lines)
        else: # 如果推文里面没有任何图片，找<p出现超过3次的行
            for line in html.split("\n"):
                if line.count("<p")>3:
                    break
            else:
                line = html #还是找不到就干脆处理整个html了 会引入一些“杂质”
        
    for tag in ["section", "figure"]: # 这两个tag需要被当成p处理
        line = line.replace("<"+tag,"<p").replace("</"+tag+">","</p>") 
    
    line = line.replace(' data-src=', ' src=')
    line = line.replace("data-croporisrc","xxx") # 干扰图片的src提取，需要替换掉
    
    md = myconvert(line, debug, api)
    
    md = "\n".join([title,"====","[source: "+nickname+"]("+url+")", md])
    
    return title, md


if __name__ == "__main__":
    debug=int(sys.argv[2])
    title, result = mpweixin_url2md(sys.argv[1], None, debug=debug)
    if debug==3:
        print(title)
        print(result)