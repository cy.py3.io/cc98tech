#!/usr/bin/env python3
import subprocess
import os
gitlog = subprocess.check_output("git log --pretty=format:%ad|%s".split()).decode()
os.chdir("githubrepo")
targetgl = subprocess.check_output("git log --pretty=format:%ad".split()).decode().split("\n")
for line in gitlog.split("\n")[::-1]:
    line = line.split("|")
    t = line[0]
    message = line[1]
    if t not in targetgl:
        print(t, message)
        cmd = ["git","commit", "-m", message, "--date", t, "--allow-empty"]
        subprocess.run(cmd)
subprocess.run(["git","push"])
