import re
from bs4 import BeautifulSoup

__all__ = ['Tomd', 'convert']

MARKDOWN = {
    'h1': ('\n# ', '\n'),
    'h2': ('\n## ', '\n'),
    'h3': ('\n### ', '\n'),
    'h4': ('\n#### ', '\n'),
    'h5': ('\n##### ', '\n'),
    'h6': ('\n###### ', '\n'),
    'code': ('`', '`'),
    'ul': ('', ''),
    'ol': ('', ''),
    'li': ('- ', ''),
    'blockquote': ('\n> ', '\n\n'),
    'em': ('**', '**'),
    'strong': ('**', '**'),
    'block_code': ('\n```\n', '\n```\n'),
    'span': ('', ''),
    'p': ('\n', '\n'),
    'table': ('\n', '\n'),
    'p_with_out_class': ('\n', '\n'),
    'inline_p': ('', ''),
    'inline_p_with_out_class': ('', ''),
    'b': ('**', '**'),
    'font': ('**', '**'),
    'i': ('*', '*'),
    'del': ('~~', '~~'),
    'hr': ('\n---', '\n\n'),
    'thead': ('\n', '|------\n'),
    'tbody': ('\n', '\n'),
    'td': ('|', ''),
    'th': ('|', ''),
    'tr': ('', '\n')
}

BlOCK_ELEMENTS = {
    'h1': '<h1.*?>(.*?)</h1>',
    'h2': '<h2.*?>(.*?)</h2>',
    'h3': '<h3.*?>(.*?)</h3>',
    'h4': '<h4.*?>(.*?)</h4>',
    'h5': '<h5.*?>(.*?)</h5>',
    'h6': '<h6.*?>(.*?)</h6>',
    'hr': '<hr/>',
    'blockquote': '<blockquote.*?>(.*?)</blockquote>',
    #'block_code': '<pre.*?><code.*?>(.*?)</code></pre>',
    'block_code': '<pre[^>]*?><code[^>]*?>(.*?)</code></pre>',
    'p': '<p\s.*?>(.*?)</p>',
    'table': '<table\s.*?>(.*?)</table>',
    'p_with_out_class': '<p>(.*?)</p>',
    'thead': '<thead.*?>(.*?)</thead>',
    'tr': '<tr>(.*?)</tr>'
}

INLINE_ELEMENTS = {
    'td': '<td>(.*?)</td>',
    'tr': '<tr>(.*?)</tr>',
    'th': '<th>(.*?)</th>',
    'b': '<b>(.*?)</b>',
    'font': '<font>(.*?)</font>',
    'i': '<i>(.*?)</i>',
    'del': '<del>(.*?)</del>',
    'inline_p': '<p\s.*?>(.*?)</p>',
    'inline_p_with_out_class': '<p>(.*?)</p>',
    'code': '<code.*?>(.*?)</code>',
    'span': '<span.*?>(.*?)</span>',
    'img': '<img.*?src="(.*?)".*?>(.*?)</img>',
    'a': '<a.*?href="(.*?)".*?>(.*?)</a>',
    'em': '<em.*?>(.*?)</em>',
    'strong': '<strong.*?>(.*?)</strong>'
}

DELETE_ELEMENTS = ['<span.*?>', '</span>', '<div.*?>', '</div>']

FIXTABLE_MAGIC = "5YBCGcnQR3HwBXK!"

CODE_PLACEHOLDER = "xa#DKeyx*n465KS7", "QewaN4EJzTn7YsD9"

class Element:
    def __init__(self, start_pos, end_pos, content, tag, is_block=False):
        self.start_pos = start_pos
        self.end_pos = end_pos
        self.content = content
        self._elements = []
        self.is_block = is_block
        self.tag = tag
        self._result = None

        if self.is_block:
            self.parse_inline()

    def __str__(self):
        wrapper = MARKDOWN.get(self.tag)
        self._result = '{}{}{}'.format(wrapper[0], self.content, wrapper[1])
        return self._result

    def parse_inline(self):
        if self.tag == "table":
            self.content = self.content.replace("\xa0"," ")
            soup = BeautifulSoup("<table>"+self.content+"</table>", "html.parser")
            result = []
            for tr in soup.find_all("tr"):
                result.append([td.text for td in tr.find_all("td")])
            cols = max([len(tr) for tr in result])
            result.insert(1,["|".join(["---"]*cols)])
            self.content = "<br>"
            for tr in result:
                self.content += "|"+"|".join(tr)+"|\n"
            self.content += "<br>"
        else:
            if "<table" in self.content:
                self.content = re.sub(BlOCK_ELEMENTS["table"], FIXTABLE_MAGIC, self.content)
            for tag, pattern in INLINE_ELEMENTS.items():
                if tag == 'a':
                    self.content = re.sub(pattern, '[\g<2>](\g<1>)', self.content)
                elif tag == 'img':
                    self.content = re.sub(pattern, '![\g<2>](\g<1>)', self.content)
                elif self.tag == 'ul' and tag == 'li':
                    self.content = "\n"+re.sub(pattern, '- \g<1>\n', self.content)+"\n"
                elif self.tag == 'ol' and tag == 'li':
                    self.content = "\n"+re.sub(pattern, '1. \g<1>\n', self.content)+"\n"
                elif self.tag == 'thead' and tag == 'tr':
                    self.content = re.sub(pattern, '\g<1>\n', self.content.replace('\n', ''))
                elif self.tag == 'tr' and tag == 'th':
                    self.content = re.sub(pattern, '|\g<1>', self.content.replace('\n', ''))
                elif self.tag == 'tr' and tag == 'td':
                    self.content = re.sub(pattern, '|\g<1>', self.content.replace('\n', ''))
                elif tag == 'code':
                    if "<code" in self.content:
                        matchtext = re.search(pattern, self.content).group(1)
                    else:
                        matchtext = self.content
                    if "\n" in matchtext or "<br" in matchtext:
                        self.content = re.sub(pattern, '```'+CODE_PLACEHOLDER[0]+'\n\g<1>```'+CODE_PLACEHOLDER[1]+'\n', self.content)
                    else:
                        wrapper = MARKDOWN.get(tag)
                        self.content = re.sub(pattern, '{}\g<1>{}'.format(wrapper[0], wrapper[1]), self.content)
                else:
                    #print("tag:",tag,"self.tag:",self.tag)
                    wrapper = MARKDOWN.get(tag)
                    self.content = re.sub(pattern, '{}\g<1>{}'.format(wrapper[0], wrapper[1]), self.content)

TAG_RE = re.compile(r'<[^>]+>')
def remove_tags(text):
    result = []
    for line in text.split("\n"):
        line = line.rstrip()
        if line.startswith(">"):
            line = line.replace("<br>","\n>").replace("<br ","\n><br ")
        else:
            line = line.replace("<br>","\n").replace("<br ","\n<br ")
        result.append(TAG_RE.sub('', line))
    return "\n".join(result)

def fix_md(md):
    md = md.replace(FIXTABLE_MAGIC, "").replace("\r","\n").replace("&nbsp;"," ")
    tmpcode = []
    result = []
    notincode = True
    mdlines = md.split("\n")
    for i in range(len(mdlines)):
        line = mdlines[i]
        if len(line)>=2:
            if line.startswith("`") and line.endswith("`"):
                tmpcode.append(line[1:-1])
                continue
            else:
                if len(tmpcode):
                    result.append('```\n' + "\n".join(tmpcode) + '\n```\n')
                    tmpcode = []
        if line.startswith("  ") and len(line.strip()) and len(line.strip()[0].encode("utf-8"))>1:
            line = "　　"+line.lstrip()
        if "```" in line:
            if notincode:
                notincode = False
            else:
                notincode = True
        if notincode:
            while "****" in line:
                line = line.replace("****","")
            if all([i=="*" for i in line.strip()]):
                continue
            if line == ">":
                if len(mdlines)>i and (not mdlines[i+1].startswith(">")):
                    line = "\n"
        result.append(line)
    result = "\n".join(result)
    result = result.strip().replace("&gt;",">").replace("&lt;","<").replace("&amp;","&").replace("\n# ![]","\n![]").replace("\n## ![]","\n![]").replace("\n**\n","\n")
    while "\n\n\n" in result:
        result = result.replace("\n\n\n","\n\n")
    result = result.replace(CODE_PLACEHOLDER[0], "").replace(CODE_PLACEHOLDER[1], "").replace("```\n`\n```","```").replace("```\n\n","```\n")
    return result

LINENUM_SPAN_RE = re.compile(r'<span class="linenum hljs-number"[^>]+>[^<]+</span>')
IFRAME_VIDEO_RE = re.compile(r'<iframe class="video_iframe"[^>]+https://v.qq.com/iframe/preview.html\?vid=([^&]+)[^>]+>[^<]*</iframe>')
DEL_SPAN_RE = re.compile(r"""<span[^>]*style=['"][^'"]*text-decoration: line-through[^>]+>([^<]*)</span>""")
def fix_html(html):
    """
    use some replacements to enhance tomd performance
    1. if there are code tags, replace linenum to ""
    2. if there are iframe video tags, replace to v.qq.com link
    """
    if "<code" in html:
        html = LINENUM_SPAN_RE.sub('', html)
    if '<iframe class="video_iframe" ' in html:
        html = IFRAME_VIDEO_RE.sub("\nhttps://v.qq.com/x/page/"+r'\1'+".html\n", html)
    if 'text-decoration: line-through' in html:
        html = DEL_SPAN_RE.sub(r"<del>\1</del>", html)
    return html

class Tomd:
    def __init__(self, html='', options=None):
        self.html = html
        self.options = options
        self._markdown = ''

    def convert(self, html, options=None):
        elements = []
        html = fix_html(html)
        if "<li" in html:
            """
            由于tomd对ol li和ul li很迷的处理，会导致列表中的部分行在生成的列表之后或之前重复出现
            为了彻底解决这个问题，这里引入BeautifulSoup得到列表的每个li元素，然后每个元素再调用convert函数得到ubbcode文本 改为p标签
            """
            soup = BeautifulSoup(html, "html5lib")
            for tag in ["ul", "ol"]:
                prefix = "- " if tag=="ul" else "1. "
                for m in soup.find_all(tag):
                    lis = m.find_all("li")
                    for li in lis:
                        new_tag = soup.new_tag("p")
                        li_html = "<p>"+("<".join((">".join(str(li).split(">")[1:])).split("<")[:-1]))+"</p>"
                        #print("convert:",li_html, convert(li_html))
                        #input("convert")
                        if "<li" in li_html or "<ul" in li_html:
                            # li with more list? just ignore it...
                            continue
                        new_tag.string = prefix + convert(li_html)
                        li.replace_with(new_tag)
                    if len(lis) == 1:
                        m.replace_with(new_tag)
                    m.name = "p"
                    #print(m)
                    #input("wait m")
            html = str(soup)
            #print(html)
        for tag, pattern in sorted(BlOCK_ELEMENTS.items()):
            #print(tag, pattern)
            for m in re.finditer(pattern, html, re.I | re.S | re.M):
                element = Element(start_pos=m.start(),
                                  end_pos=m.end(),
                                  content=''.join(m.groups()),
                                  tag=tag,
                                  is_block=True)
                #print(element)
                can_append = True
                if tag in ["ul", "ol"]:
                    if 'class="linenums' in m.group(0):
                        #ignore ol with class="linenums"
                        continue
                if tag!="table":
                    for e in elements:
                        if e.start_pos <= m.start() and e.end_pos >= m.end():
                            can_append = False
                        elif e.start_pos >= m.start() and e.end_pos <= m.end() and e.tag!="table":
                            elements.remove(e)

                if can_append:
                    if tag=="blockquote":
                        element.content = ''.join(m.groups())+"<br>\n"
                    elements.append(element)

        elements.sort(key=lambda element: element.start_pos)
        self._markdown = ''.join([str(e) for e in elements])

        for index, element in enumerate(DELETE_ELEMENTS):
            self._markdown = re.sub(element, '', self._markdown)
        
        r = self._markdown
        
        self._markdown = remove_tags(r)
        result = fix_md(self._markdown)
        return result

    @property
    def markdown(self):
        self.convert(self.html, self.options)
        return self._markdown


_inst = Tomd()
convert = _inst.convert
